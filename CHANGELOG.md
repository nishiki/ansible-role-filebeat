# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Changed

- test: replace kitchen to molecule
- chore: replace apt_key to get_url
- test: use personal docker registry

### Removed

- test: support debian 11

## v1.0.0 - 2019-11-09

### Added

- first version
