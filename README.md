# Ansible role: filebeat

[![Version](https://img.shields.io/badge/latest_version-1.0.0-green.svg)](https://code.waks.be/nishiki/ansible-role-filebeat/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-filebeat/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-bind/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-bind/actions?workflow=molecule.yml)

Install and configure filebeat

## Requirements

- Ansible >= 2.8
- Debian
  - Bookworm

## Role variables

- `filebeat_major_version` set major version to install- (default: `7`)
- `filebeat_config` - hash with the configuration (see [filebeat documentation](https://www.elastic.co/guide/en/beats/filebeat/current/configuring-howto-filebeat.html))

```
  filebeat.inputs:
    - type: log
      paths:
        - /var/log/messages
        - /var/log/*.log
  output.file:
    path: "/tmp/filebeat"
    filename: filebeat
```

## How to use

```
- hosts: server
  roles:
    - filebeat
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
