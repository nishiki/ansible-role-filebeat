import testinfra.utils.ansible_runner
import time

def test_packages(host):
  package = host.package('filebeat')
  assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/filebeat/filebeat.yml')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644

def test_service(host):
  service = host.service('filebeat')
  assert service.is_running
  assert service.is_enabled
 
def test_output(host):
  time.sleep(15)
  path = host.file('/tmp/filebeat/filebeat')
  assert path.exists
  assert path.is_file
  assert path.contains('@timestamp')
